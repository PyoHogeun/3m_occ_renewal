var dataType 	= "";
var urlVal 		= "";

if(get_version_of_IE() == "5.0" || get_version_of_IE() == "6.0" || get_version_of_IE() == "7.0" || get_version_of_IE() == "8.0" || get_version_of_IE() == "9.0"){
	dataType = "jsonp";
	urlVal = "?version="+get_version_of_IE()+"&callback=?";
}else{
	dataType = "json";
	urlVal = "?version="+get_version_of_IE();
}

//암호화작업 이벤트
function onclickEncoding(setId){
	//var params="&sendVal="+$.trim(Base64.encode($('#sendVal').val()))+"&siteKey="+$.trim(Base64.encode($("#siteKey").val()));
	//var params="&sendVal="+$.trim(Base64.encode($('#sendVal').val()));
	var params="&sendVal="+$.trim(Base64.encode($('#sendVal').val()));
	if($.trim($('#sendVal').val()) == "" || $.trim($('#sendVal').val()) == null){
		alert("패스워드 값을 확인해주세요.");
		return;
	}
	
	var strSetId = "#"+setId;
	
	$.support.cors = true;
	$.ajax({ 
		type: 'POST',
		data: params,
		dataType: dataType,
		url: '/shaEncdoing.do'+urlVal+params,
		//crossdomain:true,
		success: function(data){
			
			if(data.CODE == "100"){
				alert("인증키 실패 올바른 접속이 아닙니다.");
				return false;
			}else{
				$(strSetId).val(data.toVal);	
			}
		},
		error: function(request, status, error){
				alert("에러가 발생! 관리자에게 문의!");
		}
	});	
}

//임시패스워드 생성 이벤트
function onclickTempPasswd(setTempId, setTempEncId){
	
	var strSetTempId = "#"+setTempId;
	var strSetTempEncId = "#"+setTempEncId;
	
	$.support.cors = true;
	$.ajax({
		type: 'POST',
		dataType: dataType,
		url: '/user/member/tempPasswd.do'+urlVal,
		//crossdomain:true,
		success: function(data){
						
			if(data.CODE == "100"){
				alert("인증키 실패 올바른 접속이 아닙니다.");
				return false;
			}else{
				$(strSetTempId).val(data.tempPasswd);
				$(strSetTempEncId).val(data.tempOrgPasswd);				
			}			
		},
		error: function(request, status, error){
				alert("에러가 발생! 관리자에게 문의!");
		}
	});	
}

//IE버전 체크 이벤트~!
function get_version_of_IE () { 
	 var word; 
	 var version = "N/A"; 
	 var agent = navigator.userAgent.toLowerCase(); 
	 var name = navigator.appName; 

	 // IE old version ( IE 10 or Lower ) 
	 if ( name == "Microsoft Internet Explorer" ) word = "msie "; 
	 else { 
		 // IE 11 
		 if ( agent.search("trident") > -1 ) word = "trident/.*rv:"; 

		 // Microsoft Edge  
		 else if ( agent.search("edge/") > -1 ) word = "edge/"; 
	 } 
	 var reg = new RegExp( word + "([0-9]{1,})(\\.{0,}[0-9]{0,1})" ); 
	 if (  reg.exec( agent ) != null  ) version = RegExp.$1 + RegExp.$2; 
	 return version; 
} 
if (window.console == undefined) {console={log:function(){} };}

var ocsd = {

	init : function(){
		this.header.init();
	}

	// header
	,header : {

		op : {
			area : null,
			bg : null,
			allBtn : null,
			allMenu : null,
			gnb : null,
			even : null,
			rollng : null,
			allState : true,
			gnbState : true
		}

		,init : function(){

			this.op.area = $('#gnbArea'),
			this.op.bg = $('.gnb-bg'),
			this.op.allBtn = $('.all-product a'),
			this.op.allMenu = $('.all-menu'),
			this.op.gnb = $('.gnb'),
			this.op.even = $('.top-event'),
			this.op.rolling = $('.top-rolling');

			this.allmenu();
			this.gnb();
			this.gnbRolling();
		}

		// 전체상품보기
		,allmenu : function(){

			var area = this.op.area,
				bg = this.op.bg,
				btn = this.op.allBtn,
				menu = this.op.allMenu,
				gnb = this.op.gnb,
				gnb2Depth = gnb.find('div'),
				even = this.op.even,
				rolling = this.op.rolling,
				ease = 'easeInOutExpo',
				speed = 450;

			btn.on('click', function(){
				allmenu(ocsd.header.op.allState);
				return false;
			});

			function allmenu(st){
				if(!st){
					btn.removeClass('active');
					bg.stop().animate({ height:0 }, { queue:false, duration:speed, easing:ease, complete:function(){
						bg.removeClass('all').css('opacity',0);
						area.removeClass('all');
					} });
					menu.stop().slideUp(speed, ease);
					ocsd.header.op.allState = true;
				}else{
					if(!ocsd.header.op.gnbState) {
						headDefault();
					}
					btn.addClass('active');
					area.addClass('all');
					bg.addClass('all').css('opacity',1).stop().animate({ height:352 }, { queue:false, duration:speed, easing:ease });
					menu.stop().slideDown(speed, ease);
					ocsd.header.op.allState = false;
				}
			}

			// gnb가 열려있을 경우
			function headDefault(){
				gnb2Depth.stop().slideUp(speed, ease);
				even.stop().slideUp(speed, ease);
				rolling.stop().slideUp(speed, ease);
				ocsd.header.op.gnbState = true;
			}
		}

		// GNB
		,gnb : function(){

			var area = this.op.area,
				bg = this.op.bg,
				gnb = this.op.gnb,
				gnb2Depth = gnb.find('div'),
				btn = this.op.allBtn,
				even = this.op.even,
				rolling = this.op.rolling,
				ease = 'easeInOutExpo',
				speed = 450;

			gnb.on('mouseenter', function(){

				if(!ocsd.header.op.allState) {
					btn.trigger('click');
				}

				area.removeClass('all');
				bg.removeClass('all').css('opacity',1).stop().animate({ height:301 }, { queue:false, duration:speed, easing:ease });
				// bg.removeClass('all').stop().slideDown(speed, ease);
				gnb2Depth.stop().slideDown(speed, ease);
				even.stop().slideDown(speed, ease);
				rolling.stop().slideDown(speed, ease);

				ocsd.header.op.gnbState = false;

				return false;
			});

			area.on('mouseleave', function(){

				gnbClose();
				return false;
			});

			function gnbClose(){
				if(ocsd.header.op.allState){// 전체상품이 열려있을경우
					bg.stop().animate({ height:0 }, { queue:false, duration:speed, easing:ease, complete:function(){ bg.css('opacity',0) } });
				}
				gnb2Depth.stop().slideUp(speed, ease);
				even.stop().slideUp(speed, ease);
				rolling.stop().slideUp(speed, ease);

				ocsd.header.op.gnbState = true;
			}

		}

		// 상단 롤링
		,gnbRolling : function(){

			var rolling = this.op.rolling,
				arrow = rolling.find('> a'),
				ctrl = rolling.find('.control > a'),
				list = rolling.find('.roll-list ul'),
				listLi = list.find('li'),
				leng = listLi.length,
				wid = listLi.width(),
				state = 0,
				ease = 'easeInOutExpo',
				speed = 450;

			list.width(leng*wid);

			ctrl.on('click', function(){
				var $this = $(this),
					idx = ctrl.index($this);

				state = idx;
				animation();

				return false;
			});

			arrow.on('click', function(){
				var $this = $(this),
					idx = arrow.index($this);

				(idx==0) ? ((state==0) ? state = leng-1 : state--) : ((state==leng-1) ? state = 0 : state++) ;
				animation()

				return false;
			});

			function animation(){
				ctrl.removeClass('active');
				ctrl.eq(state).addClass('active');
				list.stop().animate({
					marginLeft:-(wid*state)
				}, {
					queue:false, duration:speed, easing:ease
				});
			}


		}

	}

	,layerPop : function(e){
		var body = $('html, body'),
			openBtn = $('.btn-layer-open'),
			dim = $('<div class="dim" style="display:block;"></div>'),
			layer = $('.layer-pop'),
			btnOk = layer.find('.btn-ok');

		// layer open
		// openBtn.on('click', function(){
			var $this = e,
				$href = $($this.attr('href'));

			autoAlign($href);
			body.addClass('modal');
			$('#container').append(dim).show();
			$href.fadeIn(250);

			// esc click
			// $(document).on('keydown', function(e){
			// 	var kc = e.keyCode?e.keyCode:e.which;
			// 	if(kc != 27) return;
			//
			// 	// btnOk.trigger('click');
			// 	layer.fadeOut(250, function(){ dim.remove(); body.removeClass('modal'); });
			// });

		// });

		// 가운데 정렬
		function autoAlign(target){
			var w = target.width(),
				h = target.height(),
				winH = $(window).height();

			// window 높이보다 클 경우 상단 고정
			if(h > winH) {
				target.addClass('absol').css({ 'margin-left' : -(w/2) });
				return;
			}
			target.css({ 'margin-top' : -(h/2), 'margin-left' : -(w/2) });
		}

		return false;
	}

	,layerPopClose : function(e){

		var body = $('html, body'),
			layer = $('#'+e),
			dim = $('.dim');

		layer.fadeOut(250, function(){ dim.remove(); body.removeClass('modal'); });
	}

}

$(function(){
	ocsd.init();
});

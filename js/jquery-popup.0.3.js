jQuery.Popup = function(options){
    this._settings = {
        popupId : 'popup',
        popupTitle : '',
        width: 800,
        left: 0,
        top: 0,
        center: false,
        closeFlag : 'N',
        popupMove: true,
        popupBackground : true,
        popup_zindex : 1000,
        _popupBackgroundId : 'popup_bg',
        reload : false,
        popupHtml : '',
        ajax : false,
		lnkUrl : '',
		lnkTarget : '',
		callback : '' 
    };

    for (var i in options) {
        if (options.hasOwnProperty(i)){
            this._settings[i] = options[i];
        }
    }
};
jQuery.Popup.prototype = {
    openPopup : function(html, id, background){
        var id = this._settings.popupId;
        var width = this._settings.width;
        var left = this._settings.left;
        var top = this._settings.top;
        var center = this._settings.center;
        var closeFlag = this._settings.closeFlag;
        var bgBody = this._settings.popupBackground;
        var title = this._settings.popupTitle;
        var move = this._settings.popupMove;
        var zindex = this._settings.popup_zindex;
        var reload = this._settings.reload;
        var html = this._settings.popupHtml;
        var ajax = this._settings.ajax;
		var lnkUrl = this._settings.lnkUrl;
		var lnkTarget = this._settings.lnkTarget;
        var callback = this._settings.callback;
        
        if(bgBody) this._addBackground();
        if($("#"+id) && reload){
            $("#"+id).remove();
        }
		
		
        if($('#'+id).size() == 0){
			var inHtml = '<div id="'+id+'" class="layerPop" style="width:' + width + 'px;z-index:'+zindex+';position:fixed;">';
				inHtml += '  <div class="layerConts">';				
				inHtml += '	<h3>' + title + '</h3>';				
				if (ajax ) {
					inHtml += '<div id="ajaxConts" class="ajaxConts"></div>';
				}else{
					inHtml += 	   html ;
				}				
				inHtml += '  </div>';
				inHtml += '  <p class="btn-close"><span class="btn"><a href="#" onclick="javascript:fnPopClose(\''+id+'\'); return false;">�ݱ�</a></span></p>';
				inHtml += '</div>';
								
        	$(inHtml).appendTo(document.body);
			if ( ajax ) this._getAjaxUrl();
           
        }	

        if(move) $("#"+id).draggable();



        var doc = document.documentElement, body = document.body;  
        
        if( center === true ){
            $("#"+id).css("left", body.clientWidth/2 - width/2);
             $("#"+id).css("top", (doc.clientHeight/2) - $(".layerpopup").height()/2 - 450).show();  
           // $("#"+id).css("top", (doc.clientHeight/2)+(doc.scrollTop+body.scrollTop) - $(".layerpopup").height()/2 - 150).show();
        }else{
            $("#"+id).css("left", left);
            $("#"+id).css("top", top); 
        }        
        if(callback) callback();
    },

    closePopup : function(id){
        var id;
        if(!id){
            id = this._settings.popupId;
        }
        
        $("#"+id).remove();
        this._removeBackground();

    },

    _addBackground: function(){ 
        var body = document.body;
        var backgroundId = this._settings._popupBackgroundId;
        var zindex	= this._settings.popup_zindex - 1;
        if($('#'+backgroundId).size()>0){
            $('#'+backgroundId).addClass("layerpopup-background");
        }else {
            $('<div id="'+backgroundId+'" class="layerpopup-background" style="position:fixed;background-color:#000;opacity:0.6;z-index:' + zindex + ';top:0;left:0;"></div>').appendTo(document.body);
        } 
        $('#'+backgroundId).width(0);
        $('#'+backgroundId).height(0);
        $('#'+backgroundId).width(body.clientWidth);
        $('#'+backgroundId).height(body.clientHeight);
    },
    _removeBackground : function(){
    	
        var backgroundId = this._settings._popupBackgroundId;
        if($(".popup").size() < 1) {
            $('#'+backgroundId).removeClass("layerpopup-background");
            $('#'+backgroundId).remove();
        }
    } , 
    _getAjaxUrl : function(){
    	var id = this._settings.popupId;
    	var doc = document.documentElement           	
        $.ajax({
            type: "get", 
            url : this._settings.lnkUrl,
            dataType : "html",  
            data : "",  
            cache:false,
            error:function ( e ){	                 
            },
            success:function ( html, status ){
    			$("#ajaxConts").html(html);
            },
            complete:function(xhr, status){
         
                if ($("#" + id).height() > doc.clientHeight ) {                	
                	$("#" + id).css("height", doc.clientHeight - 300);
                	$("#" + id).css("overflow-y", "scroll");
                }
            }
            
        });     	
    }
};
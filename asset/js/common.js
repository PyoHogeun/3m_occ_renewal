jQuery(function($) {
	// 달력 팝업 설정
	$.datepicker.regional['ko'] = {
		closeText : '닫기',
		prevText : '이전달',
		nextText : '다음달',
		currentText : '오늘',
		monthNames : [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월',
				'10월', '11월', '12월' ],
		monthNamesShort : [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월',
				'9월', '10월', '11월', '12월' ],
		dayNames : [ '일', '월', '화', '수', '목', '금', '토' ],
		dayNamesShort : [ '일', '월', '화', '수', '목', '금', '토' ],
		dayNamesMin : [ '일', '월', '화', '수', '목', '금', '토' ],
		weekHeader : 'Wk',
		dateFormat : 'yy-mm-dd',
		showOn : "button",
		buttonImage : '/cms/asset/img/date_img.jpg',
		buttonImageOnly : false,
		firstDay : 0,
		isRTL : false,
		showMonthAfterYear : true,
		yearSuffix : '',
		changeMonth : true,
		changeYear : true,
		yearRange : 'c-1:c+5',
		showOtherMonths : true,
		selectOtherMonths : false,
		showWeek : false,
		showButtonPanel : true
	};
	$.datepicker.setDefaults($.datepicker.regional['ko']);

	// jquery appendVal plugin
	$.fn.appendVal = function (newPart) {
		var result = this.each(function(){ 
			if( null != $(this).val() && "" != $(this).val() ){
				$(this).val( $(this).val() +","+ newPart );
			}else{
				$(this).val( $(this).val() + newPart); 
			}
		});
		return result;
	};
	
	$("input:checkbox[name='chkAll']").click(function(){
		if ( $(this).is(":checked") ) {
    		$("input:checkbox[name=chkSeq]").prop("checked", true);
    	}else{
    		$("input:checkbox[name=chkSeq]").prop("checked", false);
    	}			
	}); 
});

//############ 로그인 여부 확인 ################################
var isLoginLnk = (function(lnkUrl){		
	if(isLogin == false){		
		if( ! confirm("로그인이 필요한 서비스 입니다.\n로그인 페이지로 이동하시겠습니까?") ) 	return false;
		location.href = '/oralcare/m/member/login.do';
		return false;
	}else{
		if ( isBlank(lnkUrl) == false) location.href = lnkUrl;		
		return true;
	}
});	

// ############팝업 열기################################
function popWin(url, w, h, scroll, name) {
	var option = "status=no,height=" + h + ",width=" + w
			+ ",resizable=no,left=0,top=0,screenX=0,screenY=0,scrollbars="
			+ scroll;

	commonPopWin = window.open(url, name, option);
	commonPopWin.focus();
}

function confirmPopWin(url, w, h, scroll, name) {
	if (confirm("새 창으로 열립니다. 여시겠습니까?") == false)
		return;

	var option = "status=no,height=" + h + ",width=" + w
			+ ",resizable=no,left=0,top=0,screenX=0,screenY=0,scrollbars="
			+ scroll;

	commonPopWin = window.open(url, name, option);
	commonPopWin.focus();
}
function confirmTargetLocation(url) {
	if (confirm("새 창으로 열립니다. 여시겠습니까?") == false)
		return;
	var popWin = window.open('about:blank');
	popWin.location.href = url;
}
function targetLocation(url) {
	var popWin = window.open('about:blank');
	popWin.location.href = url;
}

// 아이프레임 리사이즈
function fnIframeResize(_objId) {
	var iframeObj = document.getElementById(_objId);

	var childFrameHeight = iframeObj.contentWindow.document.body.offsetHeight;

	$('#' + _objId).css('height', childFrameHeight);

}


//에디터 전역 변수
var oEditors = [];

/** 네이버 에디터 추가 * */
function addEditor(id, fnc) {
	nhn.husky.EZCreator.createInIFrame({
		oAppRef : oEditors,
		elPlaceHolder : id,
		sSkinURI : "/oralcare/editor/SmartEditor2Skin.html",
		htParams : {
			bUseToolbar : true, // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseVerticalResizer : true, // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지
			// 않음)
			bUseModeChanger : true, // 모드 탭(Editor | HTML | TEXT) 사용 여부
			// (true:사용/ false:사용하지 않음)
			fOnBeforeUnload : function() {
			},
			aAdditionalFontList : []
		}, // boolean
		fOnAppLoad : function() {
			// oEditors.getById[id].setDefaultFont('kopubM', 10) ;
			if (fnc) {
				fnc();
			}
		},
		fCreator : "createSEditor2"
	});
}

/** 네이버 에디터에서 값을 가져온다. * */
function getHTML(id) {
	//return sHTML = oEditors.getById[id].exec("UPDATE_CONTENTS_FIELD", []);
	return sHTML = oEditors.getById[id].getIR();
	
}
function setHTML(id, HTML) {
	oEditors.getById[id].exec("PASTE_HTML", [ HTML ]);
}
/** XSS 치환 return **/
function fnStripTag(str){  
	str = str.replace(/<[^<|>]*>|&nbsp;|\r\n/gi, "").trim();
	return str;
}

//페이징 이동 
function fnPaging(pageIndex){
    var frm = document.searchForm;
    $("input[name='pageIndex']").val( pageIndex );
    frm.submit();
}

//페이징 이동 Sub
function fnPagingSub(pageIndexSub){
  var frm = document.searchForm;
  $("input[name='pageIndexSub']").val( pageIndexSub );
  frm.submit();
}



//페이징 Url 이동 
function fnPagingUrl(pageIndex, _url){
    var frm = document.searchForm;
    $("input[name='pageIndex']").val( pageIndex );
    frm.action = _url;
    frm.submit();
}

//페이지 이동
function fnPage(_url) {
    var frm = document.searchForm;
    frm.action = _url;
    frm.method = "get";
    frm.submit();
}

//상세페이지 이동 
function fnView(_url, _seq) {
    var frm = document.searchForm;
	$("input[name='seq']").val( _seq );
	frm.action = _url;
	frm.method = "get";
	frm.submit();
}

// 첨부파일 화면 삭제( 단일 구조 )
function fnAtDel(obj) {
	$(obj).parent().parent().remove();
	
}
// 삭제
function fnDel(_seq) {
	if(confirm('해당 정보를 영구 삭제하시겠습니까?')){
		var frm = $("form[name=deleteForm]");
		frm.find("input[name='seq']").val( _seq );
		frm.submit();
	}
}

function fnDelAjax(_seq) {
	if(! confirm('해당 정보를 영구 삭제하시겠습니까?')) return;
	var frm = $("form[name=deleteForm]");
	frm.find("input[name='delSeq']").val( _seq );
	var params = frm.serialize();
	var url = frm.attr("action");
    $.post(
            url,
            params,
            function(data){
                if(data.result == "S" ){     
                	alert("삭제되었습니다.");
                	$("form[name=searchForm]").submit();
                }else{
                	alert("삭제중 오류가 발생했습니다. 관리자에게 문의하세요.");
				}

            }, "json"
       ); 	
}

function fnDelAjaxPwd(_seq) {
	if(! confirm('해당 정보를 영구 삭제하시겠습니까?')) return;
	var frm = $("form[name=deleteForm]");
	frm.find("input[name='delSeq']").val( _seq );
	frm.find("input[name='tempPwd']").val( $('#pwd').val() );
	if ( $('#pwd').val() == '' ){
		alert("비밀번호를 입력해 주세요.");
		return false;
	}
	var params = frm.serialize();
	var url = frm.attr("action");
    $.post(
            url,
            params,
            function(data){
                if(data.result == "S" ){     
                	alert("삭제되었습니다.");
                	$("form[name=searchForm]").submit();
                }else{
                	alert("삭제중 오류가 발생했습니다. 관리자에게 문의하세요.");
				}

            }, "json"
       ); 	
}

//선택 삭제
function fnDelArr(){
	if (! $("input[name='chkSeq']").is(":checked"))	{
		alert("선택한 내용이 없습니다.");
		return ;
	}
	if (confirm("선택한 내용을 영구 삭제하시겠습니까?") ) 	{
		var frm = $("form[name=deleteForm]");

		frm.find("input[name=seq]").val("");
		$("input[name='chkSeq']:checked").each(function(){
			frm.find("input[name=seq]").appendVal( $(this).val() );
		});		
		frm.submit();		
	}     
}

function fnDelAjaxArr() {
	if (! $("input[name='chkSeq']").is(":checked"))	{
		alert("선택한 내용이 없습니다.");
		return ;
	}	
	if(! confirm('해당 정보를 영구 삭제하시겠습니까?')) return;
	var frm = $("form[name=deleteForm]");
	frm.find("input[name=delSeq]").val("");
	$("input[name='chkSeq']:checked").each(function(){
		frm.find("input[name=delSeq]").appendVal( $(this).val() );
	});	
	var params = frm.serialize();
	var url = frm.attr("action");
    $.post(
            url,
            params,
            function(data){
                if(data.result == "S" ){
                	alert("삭제되었습니다.");
                	$("form[name=searchForm]").submit();
                }else{
                	alert("삭제중 오류가 발생했습니다. 관리자에게 문의하세요.");
				}

            }, "json"
       ); 	
}
//등록 취소
function fnCancel(_url){
	if ( ! confirm("저장없이 목록페이지로 이동하시겠습니까?") ) return;
	fnPage(_url);
	
}
// 첨부파일 다운로드
function downloadFile(path, name) {
	var url = encodeURI("/download.do?path=" + path + "&fileName=" + name);
	$("#ifr").attr("src", url);
}
// 이미지 뷰 팝업 
function imgFileView(imgSrc) {
	popWin("/imageView.do?imgSrc=" + imgSrc, 600, 500, "no", "imageView");
}
// 첨부파일 삭제
function delFile(id1, id2, obj) {
	if (id1) {
		$("#" + id1).val('');
	}
	if (id2) {
		$("#" + id2).val('');
	}
	$(obj).parent().remove();
}

// 엑셀 다운로드 
function fnExcelDown(){
	var frm = $("form[name=searchForm]");
	var params = frm.serialize();
	$("#ifr").attr("src", "downExcel.do?" + params);
}
// 주소 검색 팝업 
function fnPopAddrSearch (objId) {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
            var extraRoadAddr = ''; // 도로명 조합형 주소 변수

            // 법정동명이 있을 경우 추가한다. (법정리는 제외)
            // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
            if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                extraRoadAddr += data.bname;
            }
            // 건물명이 있고, 공동주택일 경우 추가한다.
            if(data.buildingName !== '' && data.apartment === 'Y'){
               extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
            }
            // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
            if(extraRoadAddr !== ''){
                extraRoadAddr = ' (' + extraRoadAddr + ')';
            }
            // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
            if(fullRoadAddr !== ''){
                fullRoadAddr += extraRoadAddr;
            }
			
			$("#" + objId).find("[id=zipCode]").val(data.zonecode);
			$("#" + objId).find("[id=addr2]").val(fullRoadAddr);
			$("#" + objId).find("[id=addr]").val(data.jibunAddress);
				
            


        }
    }).open();    	
}

//주소 검색 팝업 
function fnPopAddrSearch2(zipCode, addr ) {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
            var extraRoadAddr = ''; // 도로명 조합형 주소 변수

            // 법정동명이 있을 경우 추가한다. (법정리는 제외)
            // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
            if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                extraRoadAddr += data.bname;
            }
            // 건물명이 있고, 공동주택일 경우 추가한다.
            if(data.buildingName !== '' && data.apartment === 'Y'){
               extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
            }
            // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
            if(extraRoadAddr !== ''){
                extraRoadAddr = ' (' + extraRoadAddr + ')';
            }
            // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
            if(fullRoadAddr !== ''){
                fullRoadAddr += extraRoadAddr;
            }
			
			$("#" + zipCode).val(data.zonecode);
			$("#" + addr).val(fullRoadAddr);

        }
    }).open();    	
}
//############레이어  팝업################################
var showAjaxLayerPopup = (function (url, title, w, callback) {
	var PopObj = new Object();
    var popup = new jQuery.Popup({
        popupId : 'findenPop',
        popupTitle : title,
        center : true,
        popupMove: true,
        popupBackground : true, 
        ajax : true,
        lnkUrl : url,
        width:w,
        callback : callback
        				
    }); 				 	
	popup.openPopup();

});
var fnPopClose = ( function(id){
	new jQuery.Popup().closePopup("findenPop"); 
	
});

// 3자리 마다 컴마찍기
function fnSetNum(num) {
	var reg = /(^[+-]?\d+)(\d{3})/;
	num += '';
	 while (reg.test(num)) {
		 num = num.replace(reg, '$1' + ',' + '$2');
	 }
	return  num;
}

//쿠키이용
function setCookie(name, value) {
	var todayDate = new Date();
	todayDate.setDate(todayDate.getDate() + 365);   
	document.cookie = name + "=" + escape(value) + "; path=/; expires="
			+ todayDate.toGMTString() + ";"
}

//쿠키이용
function setCookies(name, value, days) {
	var todayDate = new Date();
	todayDate.setDate(todayDate.getDate() + days);
	document.cookie = name + "=" + escape(value) + "; path=/; expires="
			+ todayDate.toGMTString() + ";"
} 
// 쿠키 소멸 함수
function clearCookie(name) {
	var today = new Date()
	// 어제 날짜를 쿠키 소멸 날짜로 설정한다.
	var expire_date = new Date(today.getTime() - 60 * 60 * 24 * 1000)
	document.cookie = name + "= " + "; expires=" + expire_date.toGMTString()
}

function getCookieVal(name) {
	var nameOfCookie = name + "=";
	var x = 0;
	while (x <= document.cookie.length) {
		var y = (x + nameOfCookie.length);
		if (document.cookie.substring(x, y) == nameOfCookie) {
			if ((endOfCookie = document.cookie.indexOf(";", y)) == -1)
				endOfCookie = document.cookie.length;
			return unescape(document.cookie.substring(y, endOfCookie));
		}
		x = document.cookie.indexOf(" ", x) + 1;
		if (x == 0)
			break;
	}
	return "";
}


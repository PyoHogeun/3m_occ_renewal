if (window.console == undefined) {console={log:function(){} };}

var ocsd = {

	init : function(){
		ocsd.head.init();
		ocsd.quick();
		ocsd.layerBtnClose();
	}

	,head : {

		init : function(){
			ocsd.head.gnb();
			ocsd.head.search();
		}

		,gnb : function(){
			var body = $('html, body'),
				head = $('header'),
				menu = head.find('.menu'),
				gnbMenu = $('.gnb-menu'),
				close = gnbMenu.find('.close'),
				dim = $('.dim'),
				gnb = $('#gnb'),
				gnb1depth = gnb.find('> ul > li > a'),
				speed = 400,
				easing = 'easeInOutExpo';

			// menu open
			menu.on({
				'click' : function(){
					dim.fadeIn(200);
					body.addClass('modal');

					gnbMenu.animate({
						left : 0
					},{
						queue:false, duration:speed, easing:easing
					});

					return false;
				}
			});

			// menu close
			close.on({
				'click' : function(){
					gnbMenu.animate({
						left : '-236px'
					},{
						queue:false, duration:speed, easing:easing, complete : function(){
							dim.fadeOut(200);
							body.removeClass('modal');
						}
					});

					return false;
				}
			});

			// dim 영역 클릭 또는 좌측 스와이프시 닫기
			dim.on('click', function(){
				close.trigger('click');
			});

			// gnb toggle
			gnb1depth.on({
				'click' : function(){
					var $this = $(this),
						$parent = $this.parent('li'),
						$next = $this.next('ul');

					$parent.toggleClass('active');
					$next.stop().slideToggle(speed);

					return false;
				}
			});

		}

		,search : function(){
			var box = $('.top-search'),
				btn = box.find('.btn'),
				back = box.find('.back');

			btn.on('click', function(){
				if (box.hasClass('hide')) {
					box.removeClass('hide');
					return false;
				}
			});

			back.on('click', function(){
				box.addClass('hide');
				return false;
			});

		}
	}

	,quick : function(){
		var btnTop = $('#quickTop > a'),
			body = $('html, body');

		btnTop.on('click', function(){
			body.animate({
				scrollTop : 0
			},{
				queue:false, duration:450, easing:'easeInOutExpo'
			});
			return false;
		});
	}

	,layerPop : function(e){
		var body = $('html, body'),
			openBtn = $('.btn-layer-open'),
			dim = $('<div class="dim" style="display:block;"></div>'),
			layer = $('.layer-pop'),
			btnOk = layer.find('.btn-ok');

		// layer open
		// openBtn.on('click', function(){
			var $this = e,
				$href = $($this.attr('href'));

			autoAlign($href);
			body.addClass('modal');
			$('#contents').append(dim).show();
			$href.fadeIn(250);

			// esc click
			$(document).on('keydown', function(e){
				var kc = e.keyCode?e.keyCode:e.which;
				if(kc != 27) return;

				// btnOk.trigger('click');
				layer.fadeOut(250, function(){ dim.remove(); body.removeClass('modal'); });
			});

		// });

		// 가운데 정렬
		function autoAlign(target){
			var w = target.width(),
				h = target.height(),
				winH = $(window).height();

			// window 높이보다 클 경우 상단 고정
			if(h > winH) {
				target.addClass('absol').css({ 'margin-left' : -(w/2) });
				return;
			}
			target.css({ 'margin-top' : -(h/2) });
		}

		return false;
	}

	,layerPopClose : function(e){

		var body = $('html, body'),
			layer = $('#'+e),
			dim = $('.dim');

		layer.fadeOut(250, function(){ dim.remove(); body.removeClass('modal'); });
	}

	,layerBtnClose : function(){

		var body = $('html, body'),
			btnClose = $('.btn-close');

		btnClose.on('click', function(){

			var layer = $(this).closest('.layer-pop'),
				dim = $('.dim');

			layer.fadeOut(250, function(){ dim.remove(); body.removeClass('modal'); });

			return false;

		});


	}

}

$(function(){
	ocsd.init();
});

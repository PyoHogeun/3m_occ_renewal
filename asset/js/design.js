/* ////////////////////////////////////////////////////////////////////////////////
  Project Name         : 관리자 페이지 공개용
  Author               : 윤선상
  Written date         : 2012.09.04 ~ 
//////////////////////////////////////////////////////////////////////////////// */
try {document.execCommand('BackgroundImageCache', false, true);}catch(e){}

//jQuery가 아닌 다른 라이브러리를 사용한다면 변수할당으로 선회하세요.
//jQuery.noConflict();

var isie=(/msie/i).test(navigator.userAgent); //ie

(function($) {
    $(document).ready(function(){
  
        var url = window.location.href;
        var chkUrl = url.substr(0, url.lastIndexOf('/') + 1);          
        var test = url.substr(0, url.lastIndexOf('/')).substr(0, url.substr(0, url.lastIndexOf('/')).lastIndexOf('/'));
        var test1 = url.substr(0, url.lastIndexOf('/'));
        var oneDepthSegment = test.substr(test.lastIndexOf('/') + 1, test.length);
        var twoDepthSegment =   test1.substr(test1.lastIndexOf('/') + 1, test.length);  
        var segmentNm = "";  
        // 1depth ( 상단 )        
        var element = $('div.gnb li').filter(function() {
        	return oneDepthSegment == this.id;  
        }).addClass('on');           
        // 2depth ul 영역 show 셋팅
        var element = $('div.depth2_wrap ul > li').filter(function() {  
        	return twoDepthSegment == this.id;  
        }).addClass('on');
        
    });
})(jQuery);
